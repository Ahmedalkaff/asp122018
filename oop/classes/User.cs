﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASP122018.oop.classes
{
    class User
    {
        private static int ID_Generator = 1 ;
        public static int MAX_AGE = 150;
        public string Name;
        protected string UserName;
        private int Age;
        private string Password;
        private int _id;
        public int ID {
            get { return _id; }
            set
            {
                if (value > 0)
                    _id = value;
                else
                    _id = ID_Generator++;
            }
        }


        public int Variable;
        private int _proprity;
        public int Proprity {
            set
            {
                if (value > 0 && value < 20)
                    _proprity = value;
                else
                    _proprity = 0 ;
            }
                get { return _proprity; } }
        public bool ChangePassowrd(string pass)
        {
            if (pass != null && pass.Length >= 6)
            {
                Password = pass;
                return true;
            }
            return false;

        }
        public bool CheckPassword(string password)
        {
            return Password.Equals(password);
        }

        public void SetAge(int Age)
        {
            if (Age > 0 && Age < MAX_AGE)
                this.Age = Age;

        }

        public int GetAge() { return Age; }
        public static void StaticMethod()
        {
            MAX_AGE = 170;          // 
            // Error: Age = 10;       //  Age is not exist for static methods ,
        }
        public void InSideTheClass()
        {
            // from inside the object you can see everything non-static members
            this.Name = "";
            UserName = "";
            Age = 10;
            Password = "";
            MAX_AGE = 200;
        }
        public void foo(int Age)
        {
            this.Age = Age;
        }
    }
}
