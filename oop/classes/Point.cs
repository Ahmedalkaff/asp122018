﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASP122018.oop.classes
{
    class Point
    {
        public int x, y, z;

        public override string ToString()
        {
            return string.Format("Point ({0},{1},{2})", x, y, z);
        }
    }
}
