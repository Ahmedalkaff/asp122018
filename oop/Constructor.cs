﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP
{
    // Constructor: is a special method that  has no return data type and must have the class name
    // Constructor: is called after creating an object 
    // If no constructor is implemented the compiler will create an empty default constructor
    // if any constructor is implemented then the compiler will not create any other contrcutors for you
     // contrcutor can be overloaded 
    class A
    {
        private static int Id_generator = 1;

        private int _id; 
        public int ID { get { return _id; } private set { _id = value; } }
        public A()
        {
            _id = Id_generator++;
            Console.WriteLine(" A()");
        }
        public A(int a) {
            _id = a;
            Console.WriteLine(" A(int)");
        }

        public override string ToString()
        {
            return string.Format("A [ID:{0}]",ID);
        }
    }
  
    class Constructor
    {

        static void Main(string[] args)
        {
            A a = new A();      // this will create new object and call the Constructor

            Console.WriteLine(a);
            new A(10);
            new A(11);
            A a2 =  new A(12);
            Console.WriteLine(a2);
            Console.ReadKey();


        }

    }
}
