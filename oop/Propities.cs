﻿using ASP122018.oop.classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP
{
    class Propities
    {
        static void Main(string[] args)
        {
            User user = new User();

            user.SetAge(50);        // using set method
                                    //  user.Age = 50;          // using variables 

            user.Proprity = 10;     // each time you call Propity = value then set is executed

            Console.WriteLine(user.Proprity);
            user.Variable = 10;

            Console.ReadKey();
        }
    }
}
