﻿using ASP122018.oop.classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP
{
    
    class SettersAndGetters
    {
        static void Main(string[] args)
        {
            User user = new User();
            user.SetAge(20);
            Console.WriteLine(user.GetAge());

            user.SetAge(250);
            Console.WriteLine(user.GetAge());

            Console.ReadKey();
        }
    }
}
