﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASP122018.oop
{
    // Interface and polymorphsim
    abstract class Animal
    {
        public static int Counter { private set; get; }
        public Animal()
        {
            Counter++;
            //Console.WriteLine("An Animal object is created with ID:"+ Counter);
            Polymophizm.counter++;
            // Console.WriteLine("Animal()");
        }
        public abstract string PrintMyType();       
    }

    abstract class Mammal : Animal
    {
        public static int Counter { private set; get; }
        public Mammal()
        {
            Counter++;
            Polymophizm.counter++;
            // Console.WriteLine("Mammal()");
        }
        /*
        public override string PrintMyType()
        {
            return "Mammal";
        }*/

    }


    abstract class Bird : Animal
    {
        public static int Counter { private set; get; }
        public Bird()
        {
            Counter++;
            Polymophizm.counter++;
            //Console.WriteLine("Bird()");
        }
       /* public override string PrintMyType()
        {
            return "Bird";
        }*/
    }


    class Cat : Mammal
    {
        public static int Counter { private set; get; }
        public Cat()
        {
            Counter++;
            Polymophizm.counter++;
            // Console.WriteLine("Cat ()"); }
        }
        public override string PrintMyType()
        {
            return "Cat";
        }
    }
    class Bat : Mammal, IFlyable
    {
        public static int Counter { private set; get; }
        public Bat()
        {
            Counter++;
            Polymophizm.counter++;
            //Console.WriteLine("Bat ()"); }
        }
        public override string PrintMyType()
        {
            return "Bat";
        }
        public string CanFly()
        {
            return ", It can fly";
        }
    }


    class Penguin : Bird
    {
        public static int Counter { private set; get; }
        public Penguin()
        {
            Counter++;
            Polymophizm.counter++;
            //Console.WriteLine("Penguin ()"); }
        }
        public override string PrintMyType()
        {
            return "Penguin";
        }
    }

    class Falcon : Bird , IFlyable
    {
        public static int Counter { private set; get; }
        public Falcon()
        {
            Counter++;
            Polymophizm.counter++;
            // Console.WriteLine("Falcon ()"); }
        }

        public string CanFly()
        {
            return ", It can fly";
        }

        public override string PrintMyType()
        {
            return "Falcon";
        }
    }

    class Eagel : Bird, IFlyable
    {
        public static int Counter { private set; get; }
        public Eagel()
        {
            Counter++;
            Polymophizm.counter++;
        }
        public override string PrintMyType()
        {
            return "Eagel";
        }
        public string CanFly()
        {
            return ", It can fly";
        }
    }

    // interface : is type definder that can hold only methods proptypes (like the abstract methods)
    // those methods must be implemented by any non abstract class that inherit from this interface
    // Any class/interface can inherite from any number of interfaces 
    // Any class can inherite from only one class 
    // interfaces can't inherite from any class 


    class A :IA
    {

    }
    interface IA 
    {

    }
    interface IB :IA
    {

    }
    interface IC :IA , IB
    {

    }
    interface IFlyable
    {
         string CanFly();
    }
    class Polymophizm
    {
        public static int counter = 0;
        static void Main(string[] args)
        {
            Random rand = new Random();
            Animal[] animals = new Animal[20];

            for (int i = 0; i < animals.Length; i++)
            {
                switch (rand.Next(5))
                {
                    case 0:
                        animals[i] = new Penguin(); break;  // 3 object will be created
                    case 1:
                        animals[i] = new Falcon(); break;   // 3 object will be created
                    case 2:
                        animals[i] = new Cat(); break;      // 3 object will be created
                    case 3:
                        animals[i] = new Bat(); break;     // 3 object will be created
                    default:
                        animals[i] = new Eagel(); break;     // 3 object will be created
                   /*   case 5:
                      
                        animals[i] = new Mammal(); break;   // 1 object will be created
                    case 6:
                        animals[i] = new Bird(); break;        // 2 object will be created
                    default:
                        // animals[i] = new Animal(); break;    // 2 object will be created
                        */
                }

                //Console.Write("{0,2} ->{1}",i+1,animals[i].PrintMyType());
                //if(animals[i] is Bat)
                //    Console.WriteLine(((Bat)animals[i]).CanFly());
                //else if(animals[i] is Falcon)
                //    Console.WriteLine(((Falcon)animals[i]).CanFly());
                //else
                //    Console.WriteLine();

                Console.Write("{0,2} ->{1}", i + 1, animals[i].PrintMyType());
                if (animals[i] is IFlyable)
                    Console.WriteLine(((IFlyable)animals[i]).CanFly());
                else
                    Console.WriteLine();
            }

           // Console.WriteLine("Counter:" + counter++);


            Console.WriteLine("The number of create object of class:{0,-10} is :{1}", "Penguin", Penguin.Counter);
            Console.WriteLine("The number of create object of class:{0,-10} is :{1}", "Falcon", Falcon.Counter);
            Console.WriteLine("The number of create object of class:{0,-10} is :{1}", "Eagel", Eagel.Counter);
            Console.WriteLine("----------------------------------------------------------");
            Console.WriteLine("The number of create object of class:{0,-10} is :{1}", "Bird", Bird.Counter);
            Console.WriteLine("===========================================================");
            Console.WriteLine("The number of create object of class:{0,-10} is :{1}", "Cat", Cat.Counter);
            Console.WriteLine("The number of create object of class:{0,-10} is :{1}", "Bat", Bat.Counter);
            Console.WriteLine("----------------------------------------------------------");
            Console.WriteLine("The number of create object of class:{0,-10} is :{1}", "Mammal", Mammal.Counter);
            Console.WriteLine("===========================================================");
            Console.WriteLine("The number of create object of class:{0,-10} is :{1}", "Animal", Animal.Counter);
            Console.WriteLine("===========================================================");
            Console.WriteLine("The number of create object of class:{0,-10} is :{1}", "All", counter);

            //Falcon f = new Falcon();
            //Bird b = new Falcon();
            //Animal a = new Falcon();

            //Console.WriteLine("Falcon F ->" + f.PrintMyType());
            //Console.WriteLine("  Bird b ->" + b.PrintMyType());
            //Console.WriteLine("Animal a ->" + a.PrintMyType());
            Console.ReadKey();
        }


    }
}

