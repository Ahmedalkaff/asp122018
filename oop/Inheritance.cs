﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASP122018.oop
{
    class Person
    {
        public Person() { Console.WriteLine("Person()"); }
        public string Name { get; set; }
        public int Age { get; set; }

        public override string ToString()
        {
            return string.Format("Person [Name:\'{0}\', Age:{1}]",Name,Age);
        }
    }
    class Student : Person
    {
        public string ParentName { get { return base.Name; } set { base.Name = value; } }
        public string Name { get; set; }
        public Student() { Console.WriteLine("Student()"); }
        public double AGP { get; set; }

        // base is a refrence from each object to its parent object
        // this is a refrernce  from each object to itself 
        public override string ToString()
        {
            return string.Format("Student [Name:{2}, Person:\'{0}\', AGP:{1}]", base.ToString(), AGP,Name);
        }
    }
    class Inheritance
    {
        static void Main(string[] args)
        {
            Person p = new Person();
            p.Name = "Ahmed";
            p.Age = 35;
            Console.WriteLine("------------------");
            Person ps = new Student();
            ps.Name = "Ali";
            ps.Age = 65;
            // ps.AGP = 85; // because the variable ps is of type Person
            Console.WriteLine("------------------");
            Student s = new Student();
            s.Name = "Fadi";
            s.ParentName = "Toni";
            s.Age = 22;
            s.AGP = 95;
            
            Console.WriteLine("P  ->"+ p);
            Console.WriteLine("Ps ->" + ps);
            Console.WriteLine("s  ->" + s);
            Console.ReadKey();



        }
    }
}
