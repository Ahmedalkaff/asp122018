﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASP122018.oop
{

    enum Gender
    {
        Male = 5,
        Female 
    }
    class TheClass
    {
        public int instanceVariable;
        public static int staticVariable;
    }
    class StaticDemo
    {
        static void Main(string[] args)
        {
            // static keyword using with 

            // 1) class variables 

            TheClass.staticVariable = 10;

            TheClass obj = new TheClass();
            obj.instanceVariable = 10;

        }
    }
}
