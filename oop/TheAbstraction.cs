﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASP122018.oop
{

    // abstract class : is a class that may contain an abstract method
    // *** you can't create an object DIRECTLLY from an abstract class using new operator
    abstract class Shape
    {
        public Shape()
        {
            Console.WriteLine("An object of type Shape has been created !!");
        }
        public int ID { get; set; }
        // abstract method : is a method without any body 
        // can be exist only in an abstract  class
        public abstract double Area();
    }

    class Rectangle : Shape
    {

        public Rectangle():this(0,0) {}
        public Rectangle(int w, int h)
        {
            Width = w;
            Hieght = h;
        }
        public int Width { get; set; }
        public int Hieght { get; set; }

        public override string ToString()
        {
            return string.Format("Rectangle [Width:{0,-5},Hieght:{1,-5} ]",Width,Hieght);
        }

        public override double Area()
        {
            return Width * Hieght;
        }
    }

    abstract class Circle : Shape
    {
        
    }

    class SubCircle : Circle
    {
        public override double Area()
        {
            throw new NotImplementedException();
        }
    }
    class Triangle :Shape
    {
        public Triangle():this(0,0) { }
        public Triangle(int b, int h)
        {
            Base = b;
            Hieght = h;
        }
        public int Base { get; set; }
        public int Hieght { get; set; }

        public override string ToString()
        {
            return string.Format("Triangle  [Base :{0,-5},Hieght:{1,-5} ]", Base, Hieght);
        }

        public override double Area()
        {
            return 0.5 * Base * Hieght;
        }
    }
    class TheAbstraction
    {
        static void Main(string[] args)
        {
            Random rand = new Random();
            Shape[] shapes = new Shape[20];

            for(int i=0;i<shapes.Length;i++)
            {
                switch(rand.Next(1,3))
                {
                    case 1:
                        shapes[i] = new Rectangle(rand.Next(1,10), rand.Next(1, 10));
                        break;
                    default:
                        shapes[i] = new Triangle(rand.Next(1, 10), rand.Next(1, 10));
                        break;
                }

                Console.WriteLine("{0} with area:{1}",shapes[i],shapes[i].Area());
            }

            // can't create an object directlly using new operator of Shape class 
            // because it is an abstract class
            //Shape shape = new Shape();
            //shape.Area();           // this could not found any implementation for the Area method

            // However, you will create an object of the abstract class by creating an object 
            // of any one of  it's childern

            Shape t = new Triangle();
            Shape r = new Rectangle();
           // Shape c = new Circle();

            t.Area();
            r.Area();
            //c.Area();

            Console.ReadKey();
        }
    }
}
