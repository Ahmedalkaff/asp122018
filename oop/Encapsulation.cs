﻿using ASP122018.oop.classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP
{
    // static 
    
    class Encapsulation
    {
        private int a; 

        static void AnotherStaticMethod()
        {

        }
        static void Main(string[] args)
        {
            Encapsulation en = new Encapsulation();
            en.a = 10;

            // static members 
            User.MAX_AGE = 20;
            User.StaticMethod();


            User user = new User();
            //user.Age = 500;
            user.SetAge(500);
            Console.WriteLine(user.GetAge());

            Encapsulation.AnotherStaticMethod();
            AnotherStaticMethod();
            User.StaticMethod();
            // User.InSideTheClass();
            user.InSideTheClass();
           

            
        }
    }
}
