﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASP122018
{
    class MultiDimentionalArray
    {
        static void Main(string[] args)
        {
            // nth dimention array : is an array of n-1 th arrays 
            int[] t1 = {1,2,3 };
            int[,] t2 = { {1,2,3 }, {4,5,6 }, {7,8,9} };
            int[,,] t3 = { { {1,2 }, { 3,4}, { 4,5} }, { { 7,8}, {9,10 }, {11,12 } }, { { 13,14}, { 15,16}, { 17,18} } };
            // int[,,,,,,,,] temp = new int[24,60,60,31,12,10,10,10,10];
            int[,] matrix = new int[10, 20];
            Random rand = new Random();
            int a = 1;
            for(int r=0;r< matrix.GetLength(0);r++)
            {
                for (int c = 0; c < matrix.GetLength(1); c++)
                {
                    
                     matrix[r, c] = rand.Next(15, 35);
                    //matrix[r, c] = a++;
                     Console.Write("{0,6}, ",matrix[r,c]);
                }
                Console.Write("\b\b\n");
            }

            Console.WriteLine();


            Console.ReadKey();
            
        }
    }
}
