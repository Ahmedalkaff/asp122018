﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASP122018
{
    class Task3_RevesablesString
    {
        static void Main(string[] args)
        {
            string input , lower;
            bool IsReverable = true;
            int len, mid;
        do
            {
                Console.Write("The string to test:");
                input = Console.ReadLine();
                IsReverable = true;
                lower = input.ToLower().Trim();
                len = lower.Length - 1;
                mid = lower.Length / 2;
                
                for (int i = 0; i < mid; i++)
                {
                    if (!lower[i].Equals(lower[len - i]))
                    {
                        IsReverable = false;
                        break;
                    }
                }

                if (IsReverable)
                    Console.WriteLine("Yes, the string \"{0}\" is reversable", input);
                else
                    Console.WriteLine("No, The string \"{0}\" is NOT reversable", input);

                Console.WriteLine("Press any key to continue or Esc to exit");
            } while (! Console.ReadKey().Key.Equals(ConsoleKey.Escape));


            Console.ReadKey();
        }
    }
}
