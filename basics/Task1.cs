﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASP122018
{
    class Task1
    {
        static void Main(string[] args)
        {
            decimal d;
            Console.Write("Enter a double:");
            string input = Console.ReadLine();
            decimal.TryParse(input, out d);
            int a = Convert.ToInt32(d);         // (int)d
            decimal b = d - a;
            int c;
            int.TryParse(input.Substring(input.IndexOf('.') + 1), out c);

            Console.WriteLine("A:"+ a);
            Console.WriteLine("B:" + b);
            Console.WriteLine("C:" + c);


            Console.ReadKey();
        }
    }
}
