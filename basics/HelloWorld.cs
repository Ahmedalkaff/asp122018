﻿using System;

// #include <iostream>
namespace ASP122018
{
    class Cl
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello");
            Console.ReadKey();
        }
    }
    class HelloWorld
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World");   // cout<<"Hello World"<<endl;

            Console.Write("Console write");     // cout<<"Console write";
            Console.WriteLine(" the new statement");

           
            Console.WriteLine(10);                  // 10           (10)
            Console.WriteLine(10 + 5 + 3);          // 1053,18      (18)
            Console.WriteLine(10 + "5" + "3");      // 1053, 1053   (1053)
            Console.WriteLine(10 + "5" + 3);        // 1053,135     (1053)
            Console.WriteLine("10" + 5 + 3);        // 1053,108     (1053)
            Console.WriteLine("10" + (5 + 3));        // 1053,108   (108)
            Console.WriteLine(10 + 5 + "3");        // 1053,153     (153)

            Console.ReadKey();
        } 
    }
}
