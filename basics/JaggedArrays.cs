﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASP122018
{
    class JaggedArrays
    {
        // Jagged arrays : is an array with diffrenct dimiention lengths

        // nth dimention array : is an array of n-1 th arrays
        static void Main(string[] args)
        {
            int m = 5, n = 6, z = 4, c =1;
            // Create 2D jagged array
            int[][] jagged = new int[m][];
            for(int i=0;i<jagged.Length;i++)
            {
                jagged[i] = new int[i+1];
            }

            // Fill and print 2D jagged array

            Console.WriteLine("############# 2D jagged array ##############");

            for (int i=0;i<jagged.Length;i++)
            {
                for(int j=0;j < jagged[i].Length;j++ )
                {
                    jagged[i][j] = c++;
                    Console.Write( "{0,6}, ", jagged[i][j]);
                }
                Console.Write("\b\b  \n");
            }
            Random rand = new Random();

            // Create 3D jagged array

            int[][][] ThreeDJagged = new int[rand.Next(1,10)][][];
            for(int i=0;i<ThreeDJagged.Length;i++)
            {
                ThreeDJagged[i] = new int[rand.Next(1, 10)][];
                for (int j = 0; j < ThreeDJagged[i].Length; j++)
                    ThreeDJagged[i][j] = new int[rand.Next(1, 10)];
            }

            // Fill and print 3D jagged array

            Console.WriteLine("############# 3D jagged array ##############");
            c = 1;
            for (int i = 0; i < ThreeDJagged.Length; i++)
            {
                for (int j = 0; j < ThreeDJagged[i].Length; j++)
                {
                    for (int k = 0; k < ThreeDJagged[i][j].Length; k++)
                    {
                        ThreeDJagged[i][j][k] = c++;
                        Console.Write("{0,6}, ", ThreeDJagged[i][j][k]);
                    }
                    Console.Write("\b\b  \n");
                }
                Console.WriteLine("=========================");
            }

            Console.ReadKey();


        }
    }
}
