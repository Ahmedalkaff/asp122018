﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASP122018
{
    class SizeOF
    {
        static void Main(string[] args)
        {
            Console.WriteLine("sizeof(int) :" + sizeof(int)+ " bytes");
            Console.WriteLine("sizeof(double) :" + sizeof(double) + " bytes");
            Console.WriteLine("sizeof(decimal) :" + sizeof(decimal) + " bytes");

            Console.ReadKey();
        }
    }
}
