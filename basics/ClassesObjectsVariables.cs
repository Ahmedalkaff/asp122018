﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASP122018
{
    /// <summary>
    /// class : is a description/template/blueprint for a data type that desceibe its 
    ///        states (data field / variables and objects) and behaviours (methods)
    ///        
    /// object : is an instance of  class 
    /// 
    /// </summary>
    class ClassesObjectsVariables
    {
        class Point
        {
            int x, y;
        }
       class Person
        {

        }
        
        static void Main(string[] args)
        {
            int a = 10;
            Point p;            // only variable
            new Point();        // only object
            Point p1 = new Point();     // we have varialbe p1 referce to an object of type Point 

            Person per = new Person();

            Point[] points;                     // is a variable of type Point array 
            Point[] points1 = new Point[10];    // points1 is a variable of type Point array , referce to an object of type Array of Point that have 10 point variables 
            for (int i = 0; i < points1.Length; i++)
                points1[i] = new Point();                   // create a Point object for each variable of the 10 Point variables in the array


            int[] array = new int[3];
            array[0] = 10;
            array[1] = 20;
            array[2] = 30;

            int a1, a2, a3;
            a1 = 10;
            a2 = 20;
            a3 = 30;

            object o = new object();        // o is a variable of type object (object class) referce to an object of type (object class)
            object Class;       // what is a Class ?

            bool integer; 

        }
    }
}
