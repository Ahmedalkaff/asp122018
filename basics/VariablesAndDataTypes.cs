﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASP122018
{
   
        class VariablesAndDataTypes
        {

            static void Main(string[] args)
            {

            /// Variables : name of a refrence for a memory location 
            /// must be definde before using 
            ///     Syntax : [modifier] <DATA_TYPE> <variable_id> [=<expression>][,<variable_id> [=<expression>]]* ;
            /// 

            int x = 10 + 9, t = 54;
            Console.WriteLine("x:"+x+", t:"+ t );
   


            bool b = true;

            byte byt = 255;
            sbyte sbyt = -128;
            ushort shrt = (ushort)32767u;

            uint i = 254u;
            long log = 9223372036854775807L;

            float f = 15.32F;
            double d = 15.32;
            decimal dec = 15.32m;

            int a = 65;
            Console.WriteLine("A = " + a);        // 65
            a = 0x41;       // any number start with 0x is a hexadecimal
            Console.WriteLine("A = " + a);      // 65
            a = 'A';
            Console.WriteLine("A = " + a);      // 65
            a = '\u0041';
            Console.WriteLine("A = " + a);      // 65

            char r = '\u0041';
            Console.WriteLine("R = " + r);
            r = '\u0008';
            Console.WriteLine("this is\" :" + r + "and");


            Console.Write("Enter your age :");
            // int age = Console.Read();
          
            int age =int.Parse(Console.ReadLine());
            Console.WriteLine("Your age:" + age);

            Console.Write("Enter your age :");
            age = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Your age:" + age);
            //age = int.Parse(Console.ReadLine());
            //age = Convert.ToInt32(Console.ReadLine());

            // int.TryParse(Console.ReadLine(), out age);


            Console.Write("Enter your age :");
            int.TryParse(Console.ReadLine(), out age);

            Console.WriteLine("Your age:" + age);



            decimal input;
            Console.Write("Enter double number:");
            decimal.TryParse(Console.ReadLine(), out input);

            int a1 = (int)input;
            decimal a2 = input - a1;


            Console.WriteLine("Input :" + input);
            Console.WriteLine("a1 :" + a1);
            Console.WriteLine("a2 :" + a2);


            int m = 2;

            long l = m;         // implicit 



            int e = (int)log;

           // e = Convert.ToInt32(log);


            Console.WriteLine("Long:" + log);
            Console.WriteLine("E:" + e);


            int ma = 50 + 56, n = 2, c = 6;

                //a = 20;
                Console.Write("A :" + a);
                Console.Write("B :" + b);
                Console.Write("B :" + b);

                Console.WriteLine("A :" + b + ", B :" + b + ", C :" + c);
                Console.WriteLine("A :{0} , B :{1} , C: {2}", a, b, c);
                Console.WriteLine("A :{0,10} , B :{1,10} , C: {2,10}", a, b, c);

            Console.Write("please enter your age :");

                // a = Convert.ToInt32(Console.ReadLine()); // or 
                // a = int.Parse(Console.ReadLine());           // or 

                int.TryParse(Console.ReadLine(), out a);

                double dd = 10;
                // d = Convert.ToDouble(Console.ReadLine());
                // d = double.Parse(Console.ReadLine());
                double.TryParse(Console.ReadLine(), out d);

                Console.WriteLine("Your age is :" + a);


                bool boo = true;
                Console.WriteLine(boo);

                byte mbyte = 255;           // 0- 255
                short mshort = -32768;
                int mint = -2174;
                long mlong = -9223372036854775808;


                float mfloat = 25.32f;
                double mdouble = 25.32D;            // D is optional 
                decimal mdecimal = 25.32M;

                Console.WriteLine("Size if decimal :" + sizeof(decimal));

                Console.ReadKey();
            }
        }
    
}
