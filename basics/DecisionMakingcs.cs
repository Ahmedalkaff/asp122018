﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASP122018
{
    class DecisionMakingcs
    {
        /* Decision making techniques 
        /// 1) if statement
        ///   syntax:       
        ///                  if(<boolean expression>) 
        ///                      <statement or block>
        ///                  [else <statement or block>]
        /// 2) swtich
        ///        syntax: 
                    switch(<switch variable>)
                    {
                        [ case <unique_constant_value>:
                            [statement]*
                            break;
                        ]*
                        [default:
                            [statement]*
                            break;
                        ]
                    }
        // 3) if-else-then (ternary operator) ?: 
                syntax:
                        <boolean expression> ? value : value 
       */
        static void Main(string[] args)
        {
            /*
            int a = 10;
            if (a > 0)
            {
                Console.WriteLine("Yes");
                a++;
            }
            else
                Console.WriteLine("No");


            Console.WriteLine("Done");

            const int d = 5;

            Console.WriteLine(d - 5);
            int x = 2;
            switch (x)
            {
                //case 0:

                case 1:     //  x == 1
                case 2:
                    Console.WriteLine("First");
                    break;
                case 3:     //  x == 1
                case (d - 1):
                    Console.WriteLine("Second");
                    break;
                default:
                    Console.WriteLine("Default");
                    break;

            }

            int b;
            Console.Write("B:");
            int.TryParse(Console.ReadLine(), out b);

            if (b < 0)
                Console.WriteLine("Abs = " + -b);
            else
                Console.WriteLine("Abs = " + b);

            Console.WriteLine("Abs = " + (b < 0 ? -b : b));
            Console.WriteLine(b < 0 ? -b : b);

     */
            double mark;

            Console.Write("Enter your mark:");
            double.TryParse(Console.ReadLine(), out mark);
            if (mark < 35 || mark > 100)
           
                Console.WriteLine("Error");
            else if (mark < 50)
                Console.WriteLine("Failed");
            else if (mark < 68)
                Console.WriteLine("Passed");
            else if (mark < 76)
                Console.WriteLine("Good");
            else if (mark < 84)
                Console.WriteLine("Very Good");
            else
                Console.WriteLine("Excellent");

            Console.WriteLine(mark < 35 || mark > 100 ? "Error" : mark < 50 ? "Failed" : mark < 68 ? "Passed" : mark < 76 ? "Good" : mark < 84 ? "Very Good" : "Excellent");




            Console.ReadKey();

        }
    }
}
