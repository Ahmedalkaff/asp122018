﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASP122018
{
    class Arrays
    {
        static void Main(string[] args)
        {
            // Array : set of consecutive memory locations  

            int x1, x2, x3, x4, x5, x6, x7, x8;
            //Console.WriteLine(x1);
            //Console.WriteLine(x2);
            //Console.WriteLine(x3);

            int[] x = { 1,15, 17, 85, 2, 3, 7,20,85,34 };
            Console.Write("{");
            for (int i=0;i< x.Length; i++)
                if(i == x.Length-1)
                    Console.WriteLine(x[i] + "}");
            else
                Console.Write(x[i]+", ");


        int marks;
            Random ran = new Random();
            Console.Write("How many marks:");
            int.TryParse(Console.ReadLine(), out marks);
          
            while(marks <= 0)
            {
                Console.Write("The number of marks must larger than 0:");
                int.TryParse(Console.ReadLine(), out marks);
            }

            double[] y = new double[marks];
            int maxIndex = 0;
            int minIndex = 0;
            double minValue = Double.MaxValue;
            double maxValue = Double.MinValue;
            Console.Write("{");
            for (int i = 0; i < y.Length; i++)
            {
                y[i] = ran.Next(35,101)+ ran.NextDouble();
                y[i] = y[i] > 100 ? 100 : y[i];
                Console.Write("{0,6:F2}, ",y[i] );

                if (y[i] > y[maxIndex])
                    maxIndex = i;
                if (y[i] < y[minIndex])
                    minIndex = i;

                if (y[i] > maxValue)
                    maxValue = y[i];
                if (y[i] < minValue)
                    minValue = y[i];

            }
            Console.Write("\b\b}\n");
            Console.WriteLine("Min:{0,6:F2}, Max: {1,6:F2}", y.Min(), y.Max());
            Console.WriteLine("Min:{0,6:F2}, Max: {1,6:F2}", y[minIndex], y[maxIndex]);
            Console.WriteLine("Min:{0,6:F2}, Max: {1,6:F2}", minValue, maxValue);

            Console.ReadKey();

        }
    }
}
