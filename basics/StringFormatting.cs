﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASP122018
{
    class StringFormatting
    {
        static void Main(string[] args)
        {
            double a = 152.254, b= 15478.996654, c =-654522.2447;

            Console.WriteLine("A :" + b + ", B :" + b + ", C :" + c);
            Console.WriteLine("A :{0} , B :{1} , C: {2}", a, b, c);
            Console.WriteLine("A :{0,10} , B :{1,10} , C: {2,10}", a, b, c);
            Console.WriteLine("A :{0,-10} , B :{1,-10} , C: {2,-10}", a, b, c);
            Console.WriteLine("A :{0,-10} ,\nB :{1,-10} ,\nC: {2,-10}\n", a, b, c);
            Console.WriteLine("A :{0,10} ,\nB :{1,10} ,\nC: {2,10}\n", a, b, c);
            Console.WriteLine(c.ToString("F2"));
            Console.WriteLine(c.ToString("F4"));
            Console.WriteLine(c.ToString("E2"));
            Console.WriteLine(c.ToString("G"));
            Console.WriteLine(c.ToString("P2"));

            CultureInfo myCIintl = new CultureInfo("en-Us", false);
            Console.WriteLine("A :{0,20:C2} ,\nB :{1,20:C2} ,\nC: {2,20:C2}\n", a, b, c);

            Console.WriteLine("A :{0,20:C2} ,\nB :{1,20:C2} ,\nC: {2,20:C2}\n", a.ToString("C", myCIintl), b.ToString("C", myCIintl), c.ToString("C", myCIintl));
            Console.ReadKey();
        }
    }
}
