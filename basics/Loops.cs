﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASP122018
{
    /// <Loops>
    /// 
    /// Loops : repeat statement(s) many time based on a condition
    /// 
    /// loop techniques 
    /// 1) while
    /// suntax:
    ///             while(<boolean_expression>)
    ///                  statement or block { [statement(s)]*}
    /// 2) do while
    /// syntax:
    ///        do 
    ///         statement or block { [statement(s)]*}
    ///        while(<boolean_expression>);
    /// 3) for
    /// syntax:
    ///         for([<statement list>];[<boolean_expression>:true];[<statement list>]) 
    ///           statement or block { [statement(s)]*}
    /// 4) foreach
    ///  syntax:
    ///        foreach(<variable> in <Iteratable>)
    ///         statement or block { [statement(s)]*}
    /// </Loops>
    class Loops
    {
        static void Main(string[] args)
        {
            //int a = 0;
            //// you can write the for loop like this 
            //for (;;)
            //    ;
            //// or 
            //for (Console.WriteLine("Hello"), Console.WriteLine("Hi"); ;)
            //    Console.WriteLine(a);
            //while (true)
            //{

            //}

            //for(int a=0;a<=10;a++)
            //    Console.WriteLine(10-a);


            sbyte i = 10;
            for (i = 0; i < 10;) 
                Console.WriteLine(i--);

            Console.WriteLine("Complteted with I:" + i);

            Console.ReadKey();
        }
    }
}
