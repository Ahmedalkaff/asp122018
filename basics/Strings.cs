﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASP122018
{
    class Strings
    {
        static void Main(string[] args)
        {
            // string is an array of characters

            string str1 = "String";
            char[] letters = { 'S', 't', 'r', 'i', 'n', 'g' };
            string str2 = new string(letters);

            Console.WriteLine("String 1:"+ str1);
            Console.WriteLine("String 2:" + str2);

            Console.WriteLine(str1.CompareTo(str2));
            Console.WriteLine(str1.CompareTo("sring"));
            Console.WriteLine(str1.CompareTo("string"));
            Console.WriteLine("string".CompareTo(str1));


            Console.WriteLine(str1.Contains("r"));
            Console.WriteLine(str1.Equals(str2));
            Console.WriteLine(str1 == str2);

            Console.WriteLine(str1[1]);
            Console.WriteLine(str1[0]>= 'a' && str1[0] <= 'z' ? "Small" : str1[0] >= 'A' && str1[0] <= 'Z'? "Capital":"Other");
            Console.WriteLine(str1[1] >= 'a' && str1[1] <= 'z' ? "Small" : str1[1] >= 'A' && str1[1] <= 'Z' ? "Capital" : "Other");
            Console.WriteLine(char.IsLetter(str1[0]) ? (char.IsUpper(str1[0]) ? "Upper": "Lower") : "Not Letter");

            Console.Write("What is your name? ");
            string name = Console.ReadLine();
            Console.WriteLine("Welcome "+ name);
            Console.WriteLine(name.ToUpper());
  
            Console.ReadKey();
        }
    }
}
