﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASP122018
{
    /// <method>
    /// Method: is named block of code with return data type and optional parameters 
    /// Syntax: 1) to defind a method
    ///      
    ///  [modifier] [spcifier] <return_data_type> <method_name> ([parameter list]) {[body] }
    /// 
    /// * methods in C# MUST be definded inside a class  
    /// 
    /// Syntax:2) call a method
    ///            [class./object.]method_name([argument_list]);
    ///            
    /// 
    /// return statement : can be in any method to indicate what is the value to be returned 
    /// return statement : ends the execution of method 
    /// </summary>

    
    class MethodDemo
    {
        void method(){
            Console.WriteLine("Method");
        }
        void method(int value)
        {
            //return;         // return statement ends the method 
            Console.WriteLine("Method with value:" + value);

        }

        int sum(int a, int b)       // a = 10 , b = 20 
        {
            int sum = a + b;        // 10 + 20 = 30
            return sum;
        }

        void DoubleValue(int a)         // a = d ; // pass by value
        {
            a *= 2;
            Console.WriteLine("in method:"+a);         // 40
        }

        void DoubleValue(ref int a)         // a = d ; // pass by reference
        {
            a *= 2;
            Console.WriteLine("in method:" + a);         // 40
        }

        void DoubleValue(int[] a)        
        {
            for(int i=0;i<a.Length;i++)
                a[i] += 2;

            Print(a, "In method");
            //Console.WriteLine("in method:" + a);         // 40
        }

    // =====================================================

        class A
        {
            public int value;
        }
        void DoubleValue(A a)
        {
            a.value *= 2;
            Console.WriteLine("in method:" + a.value);         // 40
        }




        // =====================================================



        void DoubleValueWithNoChangeOnOriginal(int[] a)
        {
            int[] temp = new int[a.Length];
            for (int i = 0; i < a.Length; i++)
            {
                temp[i] = a[i];
                temp[i] += 2;
            }
              

            Print(temp, "In method");
            //Console.WriteLine("in method:" + a);         // 40
        }

        void Print(int[] arr, string name)
        {
            Console.Write("{0,-10}[",name);
            for (int i = 0; i < arr.Length; i++)
                Console.Write("{0,3}, ", arr[i]);

            Console.Write("\b\b ] \n");
        }



        static void Main(string[] args)
        {
            int d = 20;
            MethodDemo obj = new MethodDemo();
            obj.method();
            obj.method(10);
            obj.method(d);
            int r = obj.sum(10 , d);            // 30 ;
            Console.WriteLine("R:"+ r );
            Console.WriteLine(obj.sum(10, d));

            Console.WriteLine("Before D:" + d);     // 20
            obj.DoubleValue(d);     
            Console.WriteLine("After D:"+d);          //  20  

            obj.DoubleValue(ref d);
            Console.WriteLine("After D:" + d);

            
            int[] array = new int[20];
           
            obj.Print(array, "Befor");
            obj.DoubleValue(array);

            obj.Print(array, "After");

            Console.WriteLine("======================");
            obj.DoubleValueWithNoChangeOnOriginal(array);
            obj.Print(array, "After");

            // Error Console.WriteLine(obj.method());            // method() return void , can't print void 


            A o = new A();
            o.value = 20;
            Console.WriteLine("Before Value:" + o.value);       //   20
            obj.DoubleValue(o);                             // in method 40 
            Console.WriteLine("After D:" + o.value);            // 40
            Console.ReadKey();
        }

       
    }
}
