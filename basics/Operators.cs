﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASP_10_2018
{
    class Operators
    {
        static void Main(string[] args)
        {
            int a = 5, b = 17;
            Console.WriteLine(a + b);      // 22
            Console.WriteLine("10" + b);    // 1017
            Console.WriteLine(b + "10");    // 1710
            Console.WriteLine("10" + "20"); // 1020

            Console.WriteLine("10" + a + b);   // 10517
            Console.WriteLine(a + b + "10");  // 2210


            Console.WriteLine(a++); // 5
            Console.WriteLine(a);   // 6
            Console.WriteLine(++a); // 7
            // Console.WriteLine(a && b);
            Console.WriteLine(a > 0 && b != 5);
            Console.WriteLine(a > 0 || b != 5);
            Console.WriteLine(!(b != 5));

            a = 5;
            b = 17;
            Console.WriteLine(a & b);  // 1      // bool/integer & bool/integer => bool/integer
            Console.WriteLine(a | b);   // 21
            Console.WriteLine(a ^ b);  //  20

            a = 5;
            b = 17;
            // in logical opreators , compiler uses the short circutes
            // 
            Console.WriteLine(a > 10 && b++ != 3);  // false            // b++ != 3 will NOT be evaluated at all because  a > 10 is false
            Console.WriteLine(b);                   // 17


            // in bitwise operator no short circutes are used 
            Console.WriteLine(a > 10 & b++ != 3);   // false
            Console.WriteLine(b);                   // 18

            Console.WriteLine(~b);          // -19

            a = 17;
            b = 5;
            // b = 13 > , < , >= , <= 

            Console.WriteLine(true && 5 < 2);
            Console.WriteLine("=======================================================================================");

            Console.WriteLine("{0,10} {1,5} --> :{2}", "a", a, String.Format("{0,32}", Convert.ToString(a, 2)).Replace(" ", "0"));
            Console.WriteLine("{0,10} {1,5} --> :{2}", "~a", ~a, String.Format("{0,32}", Convert.ToString(~a, 2)).Replace(" ", "0"));
            Console.WriteLine("{0,10} {1,5} --> :{2}", "b", b, String.Format("{0,32}", Convert.ToString(b, 2)).Replace(" ", "0"));

            Console.WriteLine("----------------------------------------------------------------------------------------");
            Console.WriteLine("{0,10} --> {1,5} :{2}", "a | b", a | b, String.Format("{0,32}", Convert.ToString(a | b, 2)).Replace(" ", "0"));
            Console.WriteLine("{0,10} --> {1,5} :{2}", "a & b", a & b, String.Format("{0,32}", Convert.ToString(a & b, 2)).Replace(" ", "0"));
            Console.WriteLine("{0,10} --> {1,5} :{2}", "a ^ b", a ^ b, String.Format("{0,32}", Convert.ToString(a ^ b, 2)).Replace(" ", "0"));
            Console.WriteLine("=======================================( >> shift right)================================================");
            Console.WriteLine("{0,10} --> {1,5} :{2}", "a >> 0", a >> 0, String.Format("{0,32}", Convert.ToString(a >> 0, 2)).Replace(" ", "0"));
            Console.WriteLine("{0,10} --> {1,5} :{2}", "a >> 1", a >> 1, String.Format("{0,32}", Convert.ToString(a >> 1, 2)).Replace(" ", "0"));
            Console.WriteLine("{0,10} --> {1,5} :{2}", "a >> 2", a >> 2, String.Format("{0,32}", Convert.ToString(a >> 2, 2)).Replace(" ", "0"));
            Console.WriteLine("{0,10} --> {1,5} :{2}", "a >> 3", a >> 3, String.Format("{0,32}", Convert.ToString(a >> 3, 2)).Replace(" ", "0"));
            Console.WriteLine("{0,10} --> {1,5} :{2}", "a >> 4", a >> 4, String.Format("{0,32}", Convert.ToString(a >> 4, 2)).Replace(" ", "0"));
            Console.WriteLine("=======================================( << shift left )================================================");
            Console.WriteLine("{0,10} --> {1,5} :{2}", "a << 0", a << 0, String.Format("{0,32}", Convert.ToString(a << 0, 2)).Replace(" ", "0"));
            Console.WriteLine("{0,10} --> {1,5} :{2}", "a << 1", a << 1, String.Format("{0,32}", Convert.ToString(a << 1, 2)).Replace(" ", "0"));
            Console.WriteLine("{0,10} --> {1,5} :{2}", "a << 2", a << 2, String.Format("{0,32}", Convert.ToString(a << 2, 2)).Replace(" ", "0"));
            Console.WriteLine("{0,10} --> {1,5} :{2}", "a << 3", a << 3, String.Format("{0,32}", Convert.ToString(a << 3, 2)).Replace(" ", "0"));
            Console.WriteLine("{0,10} --> {1,5} :{2}", "a << 4", a << 4, String.Format("{0,32}", Convert.ToString(a << 4, 2)).Replace(" ", "0"));

            int c = 10;
            c *= 3 + 5;         // c = c * ( 3 + 5 )
            Console.WriteLine("C:" + c);

            a = 2;
            b = 3;
            c = 3 * a + b * 2 * (a + b) / 3;    // 16
          
            Console.WriteLine("C:" + c);

           
            c = 3 * ++a + ++b * 2 * (++a + ++b) / 3;
            Console.WriteLine("C:" + c);        // 35, 


            c = 10;
            a = 3;
            c *= a + 2;     // c = c * (a + 2)
            Console.WriteLine("C:" + c);        // 32, 

            Console.ReadKey();
        }
    }
}
