﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASP122018
{
    class ConsoleRead
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Read:"+ Console.Read());    // A --> 65
            Console.WriteLine("ReadLine:" + Console.ReadLine());    // read single line as string 123 --> 123

            Console.WriteLine(Console.ReadKey().Key.Equals(ConsoleKey.Escape));
            Console.WriteLine(Console.ReadKey().Key.Equals(ConsoleKey.Escape));

            Console.ReadKey();

        }
    }
}
